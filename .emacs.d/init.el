;;; Emacs config

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Prerequisites

(let ((minver "26.1"))
  (when (version< emacs-version minver)
    (error "Your Emacs is too old -- this config requires v%s or higher" minver)))

;;; Speed up init.
;;; Temporarily reduce garbage collection during startup. Inspect `gcs-done'.
(defun ambrevar/reset-gc-cons-threshold ()
  (setq gc-cons-threshold (car (get 'gc-cons-threshold 'standard-value))))
(setq gc-cons-threshold (* 64 1024 1024))
(add-hook 'after-init-hook 'ambrevar/reset-gc-cons-threshold)
;;; Temporarily disable the file name handler.
(setq default-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)
(defun ambrevar/reset-file-name-handler-alist ()
  (setq file-name-handler-alist
        (append default-file-name-handler-alist
                file-name-handler-alist))
  (cl-delete-duplicates file-name-handler-alist :test 'equal))
(add-hook 'after-init-hook 'ambrevar/reset-file-name-handler-alist)

;;; Avoid the "loaded old bytecode instead of newer source" pitfall.
(setq load-prefer-newer t)

;;; Store additional config in a 'lisp' subfolder and add it to the load path so
;;; that `require' can find the files.
;;; This must be done before moving `user-emacs-directory'.
(add-to-list 'load-path (expand-file-name "lisp/" user-emacs-directory))

;;; Move user-emacs-directory so that user files don't mix with cache files.
(setq user-emacs-directory "~/.cache/emacs/")
;; To start an Emacs with "clean" user files, set this variable to a new location.
;; (setq user-emacs-directory "~/fosdem/emacs/")

;; Tor / Proxy: set up before package initialization.
(require 'functions)                    ; Needed for `ambrevar/toggle-proxy'.
(ambrevar/toggle-proxy)

(when (require 'package nil t)
  ;; Different Emacs versions have different byte code.  If a versioned ELPA
  ;; directory is found, use it.
  (let ((versioned-dir (format "elpa-%s.%s" emacs-major-version emacs-minor-version)))
    (when (member versioned-dir (directory-files (expand-file-name ".." package-user-dir)))
      (setq package-user-dir (expand-file-name (concat "../" versioned-dir) package-user-dir))))
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
  (package-initialize))

;;; Site Lisp folder for local packages and development.
;; We need to roll it out manually since we want it first in the `load-path',
;; while `normal-top-level-add-subdirs-to-load-path' appends it to the very end.
(defun ambrevar/package-refresh-load-path (path)
  "Add every non-hidden sub-folder of PATH to `load-path'."
  (when (file-directory-p path)
    (dolist (dir (directory-files path t "^[^\\.]"))
      (when (file-directory-p dir)
        (setq load-path (add-to-list 'load-path dir))
        (dolist (subdir (directory-files dir t "^[^\\.]"))
          (when (file-directory-p subdir)
            (setq load-path (add-to-list 'load-path subdir))))))))
(let ((site-lisp (expand-file-name "site-lisp/" "~/.local/share/emacs/")))
  (add-to-list 'load-path site-lisp)
  (ambrevar/package-refresh-load-path site-lisp))

;;; Local config.  See below for an example usage.
(load "local-before" t)

(require 'hook-functions)
(require 'main)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'auto-mode-alist (cons "\\.adoc\\'" 'adoc-mode)):w


(add-to-list 'load-path "/usr/share/asymptote")
(autoload 'asy-mode "asy-mode" "Asymptote major mode." t)
(autoload 'lasy-mode "asy-mode" "Hybrid Asymptote/Latex major mode." t)
(autoload 'asy-insinuate-latex "asy-mode" "Asymptote insinuate LaTeX." t)
(add-to-list 'auto-mode-alist '("\\.asy$" . asy-mode))

(setq bibtex-entry-format '(opts-or-alts required-fields numerical-fields whitespace realign last-comma delimiters braces sort-fields))
(setq bibtex-field-delimiters 'double-quotes)
(add-hook 'bibtex-mode-hook 'ambrevar/turn-off-indent-tabs)

;; TODO: Fix gtk-look to support in-buffer documentation display with eww
;; instead of just with w3m.
(with-eval-after-load 'cc-mode (require 'init-cc))

(when (require 'company nil t)
  (setq company-idle-delay nil))

(with-eval-after-load 'debbugs
  (setq debbugs-gnu-all-severities t))

;;; TODO: In diff-mode, both `[[` and `C-M-a` do not go back to previous index
;;; once they are at the beginning of an index.

;; dired-du is mostly superseeded by disk-usage.el
;; See https://github.com/calancha/dired-du/issues/2.
;;; Dired is loaded after init.el, so configure it only then.
(with-eval-after-load 'dired (require 'init-dired))

(with-eval-after-load 'emms (require 'init-emms))

(when (require 'emojify nil :noerror)
  (global-emojify-mode))

(when (require 'engine-mode nil t)
  (require 'init-engine))

(setq evil-want-keybinding nil
      evil-want-integration t)
(when (require 'evil nil t) (require 'init-evil))

(with-eval-after-load 'eshell (require 'init-eshell))
(with-eval-after-load 'shell (require 'init-shell))

(with-eval-after-load 'eww (require 'init-eww))

(when  (require 'expand-region nil t)
  (global-set-key (kbd "C-=") 'er/expand-region))

(with-eval-after-load 'gnus (require 'init-gnus))

(with-eval-after-load 'go-mode (require 'init-go))

;;; Graphviz-dot-mode: The view command is broken but the preview command works
;;; (it displays the PNG in a new window), which is enough and probably not
;;; worth a fix.

(with-eval-after-load 'gud (require 'init-gud)) ; Also GDB.

(when (executable-find "guix")
  (require 'init-guix))

(when (require 'helm-config nil t) (require 'init-helm))

(when (require 'helpful nil t)
  (global-set-key (kbd "C-h f") #'helpful-callable)
  (global-set-key (kbd "C-h v") #'helpful-variable)
  (global-set-key (kbd "C-h o") #'helpful-at-point)
  (global-set-key (kbd "C-h F") #'helpful-function)
  (global-set-key (kbd "C-h c") #'helpful-key))

(when (require 'hl-todo nil t)
  (add-to-list 'hl-todo-keyword-faces `("REVIEW" . ,(alist-get "TODO" hl-todo-keyword-faces nil nil 'equal)))
  (global-hl-todo-mode)
  ;; (global-set-key (kbd "M-s M-o") 'hl-todo-occur)
  (define-key hl-todo-mode-map (kbd "M-s t") 'hl-todo-occur))

(when (require 'iedit nil t)
  (global-set-key (kbd "C-;") 'iedit-mode))

;;; Image
;;; TODO: Disable white frame.
;;; I think it's the cursor.
;;; Evil-mode reverts cursor changes.
;; (set-face-foreground 'cursor "black")
;;; TODO: Implement other sxiv features:
;;; - Gamma
;;; - Marks
;;; - Gallery
;;; TODO: Is it possible to display an image fullscreen?
;;; TODO: Image+: Do no auto-adjust animated files
;;; https://github.com/mhayashi1120/Emacs-imagex/issues/10
;;; TODO: Image+: Restore animation state
;;; https://github.com/mhayashi1120/Emacs-imagex/issues/9
(with-eval-after-load 'image
  (setq image-animate-loop t)
  (add-hook 'image-mode-hook 'image-toggle-animation)
  (require 'image+ nil t))

(when (require 'info-colors nil t)
  (add-hook 'Info-selection-hook 'info-colors-fontify-node))
(when (require 'helm-selector nil :noerror)
  (global-set-key (kbd "C-h i") 'helm-selector-info))

(add-hook 'js-mode-hook (lambda () (defvaralias 'js-indent-level 'tab-width)))

(with-eval-after-load 'lisp-mode (require 'init-lisp))
(with-eval-after-load 'scheme (require 'init-scheme))
(with-eval-after-load 'racket-mode (require 'init-racket))
(with-eval-after-load 'clojure-mode (require 'init-clojure))

(setq geiser-repl-history-filename (expand-file-name "geiser_history" user-emacs-directory))
(setq geiser-guile-debug-show-bt-p t)
;; Emacs Lisp
(add-hook 'emacs-lisp-mode-hook 'ambrevar/turn-on-complete-filename)
(add-hook 'emacs-lisp-mode-hook 'ambrevar/turn-on-tab-width-to-8) ; Because some existing code uses tabs.
(add-hook 'emacs-lisp-mode-hook 'ambrevar/turn-off-indent-tabs)   ; Should not use tabs.
(add-hook 'emacs-lisp-mode-hook 'ambrevar/init-lispy)
(when (fboundp 'rainbow-delimiters-mode)
  (add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode))
(ambrevar/define-keys emacs-lisp-mode-map
                      "<f5>" 'package-lint-current-buffer
                      ;; Do not use `recompile' since we want to change the compilation folder for the current buffer.
                      "<f6>" (lambda () (interactive) (async-byte-recompile-directory (file-name-directory (buffer-file-name)))))

(with-eval-after-load 'lua-mode (require 'init-lua))

;;; Magit can be loaded just-in-time.
(with-eval-after-load 'magit
  (ambrevar/define-keys magit-mode-map
                        "C-<f6>" 'compile
                        ;; Do not use `recompile' since we want to change the compilation folder for the current buffer.
                        "<f6>" 'ambrevar/compile-last-command)
  (setq auto-revert-mode-text "")
  (set-face-foreground 'magit-branch-remote "orange red")
  (setq git-commit-summary-max-length fill-column)
  ;; Customize what to fold by default.
  ;; (push (cons [* commitbuf] 'hide) magit-section-initial-visibility-alist)
  ;; Avoid conflict with WM.
  (define-key magit-mode-map (kbd "s-<tab>") nil)
  (setq magit-diff-refine-hunk 'all)
  (require 'init-magit)
  ;; magit-todos can be slow on big projects.  Use it manually.
  ;; (when (require 'magit-todos nil 'noerror)
  ;;   (magit-todos-mode))
  (require 'forge nil 'noerror))
(when (fboundp 'magit-status)
  (global-set-key (kbd "C-x g") 'magit-status))
(with-eval-after-load 'orgit
  (setq orgit-store-repository-id t))

;;; Mail
;; (with-eval-after-load 'mu4e
;;   ;; mu4e-conversation must be enabled here.
;;   ;; REVIEW: https://github.com/djcb/mu/issues/1258
;;   (when (require 'mu4e-conversation nil t)
;;     (global-mu4e-conversation-mode)
;;     ;; (setq mu4e-debug t)
;;     (setq mu4e-headers-show-threads nil
;;           mu4e-headers-include-related nil)
;;     ;; Tree is better to detect thread-jacks.
;;     (setq mu4e-conversation-print-function 'mu4e-conversation-print-tree)
;;     (add-hook 'mu4e-conversation-hook 'flyspell-mode)
;;     (defun ambrevar/mu4e-conversation-sync ()
;;       (let ((mu4e-get-mail-command "mbsync mail-sent atlas-sent"))
;;         (mu4e-update-mail-and-index 'run-in-background)))
;;     (add-hook 'mu4e-conversation-after-send-hook #'ambrevar/mu4e-conversation-sync)
;;     (add-hook 'mu4e-view-mode-hook 'auto-fill-mode))
;;   (require 'init-mu4e))
;; (autoload 'helm-mu4e-switch "mu4e")

(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
(with-eval-after-load 'nov (require 'init-nov))

(with-eval-after-load 'notmuch (require 'init-notmuch))

(with-eval-after-load 'make-mode (require 'init-makefile))

(with-eval-after-load 'markdown-mode (require 'init-markdown))

;; (add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode)) ; matlab, but conflicts with obj-c.
(defun ambrevar/octave-set-comment-start ()
  "Set comment character to '%' to be Matlab-compatible."
  (set (make-local-variable 'comment-start) "% "))
(add-hook 'octave-mode-hook 'ambrevar/octave-set-comment-start)

(autoload 'maxima-mode "maxima" "Maxima mode" t)
(autoload 'maxima "maxima" "Maxima interaction" t)
(add-to-list 'auto-mode-alist '("\\.mac" . maxima-mode))

(add-to-list 'auto-mode-alist '("\\.wiki\\'" . mediawiki-mode))
(with-eval-after-load 'mediawiki (require 'init-mediawiki))

(with-eval-after-load 'elfeed (require 'init-elfeed))

(with-eval-after-load 'org (require 'init-org))

;;; pdf-tools requires poppler built with cairo support.
;;; We cannot defer loading as `pdf-tools-install' is required for PDF
;;; association.
;;; REVIEW: `save-place' does not seem to work with pdf-tools.
;;; See https://github.com/politza/pdf-tools/issues/18.
(when (require 'pdf-tools nil t)
  ;; (setq pdf-view-midnight-colors '("#ffffff" . "#000000"))
  (setq pdf-view-midnight-colors '("#ff9900" . "#0a0a12" )) ; Amber
  ;; (add-hook 'pdf-view-mode-hook 'pdf-view-midnight-minor-mode) ; Most PDFs are not meant for this, so maybe don't set as default.
  (pdf-tools-install t t t))

(when (require 'rainbow-mode nil t)
  (dolist (hook '(css-mode-hook html-mode-hook sass-mode-hook))
    (add-hook hook 'rainbow-mode)))

(with-eval-after-load 'restclient
  (define-key restclient-mode-map (kbd "M-s f") 'helm-restclient)
  (add-to-list 'helm-source-names-using-follow "Sources")
  (with-eval-after-load 'company-restclient
    (add-to-list 'company-backends 'company-restclient)
    (add-hook 'restclient-mode-hook 'company-mode)
    (define-key restclient-mode-map (kbd "M-<tab>") (if (require 'helm-company nil t)
                                                        'helm-company
                                                      'company-complete))))

;;; Screencast
(with-eval-after-load 'camcorder
  (setq camcorder-output-directory (expand-file-name "Downloads" "~")
        camcorder-gif-output-directory camcorder-output-directory)
  (setq camcorder-recording-command '("recordmydesktop" " --fps 10 --no-sound --windowid " window-id " -o " file))
  (add-to-list 'camcorder-gif-conversion-commands '("ffmpeg-slow" "ffmpeg -i " input-file " -vf 'fps=10,scale=1024:-1:flags=lanczos' " gif-file)))
(with-eval-after-load 'gif-screencast
  (define-key gif-screencast-mode-map (kbd "<f8>") 'gif-screencast-toggle-pause)
  (define-key gif-screencast-mode-map (kbd "<f9>") 'gif-screencast-stop))

;;; Shell
(with-eval-after-load 'sh-script (require 'init-sh))

;;; Srt (subtitles)
(add-to-list 'auto-mode-alist '("\\.srt\\'" . text-mode))

;;; System packages
(global-set-key (kbd "C-x c #") 'helm-system-packages)

(with-eval-after-load 'term
  ;; (require 'init-term)
  (setq term-buffer-maximum-size 0))

(with-eval-after-load 'tex (require 'init-tex))
;; LaTeX is defined in the same file as TeX.  To separate the loading, we add it
;; to the hook.
(add-hook 'latex-mode-hook (lambda () (require 'init-latex)))

(with-eval-after-load 'transmission
  ;; `transmission' will fail to start and will not run any hook if the daemon
  ;; is not up yet.
  ;; We need to advice the function :before to guarantee it starts.
  (defun ambrevar/transmission-start-daemon ()
    (unless (member "transmission-da"
                    (mapcar
                     (lambda (pid) (alist-get 'comm (process-attributes pid)))
                     (list-system-processes)))
      (call-process "transmission-daemon")
      (sleep-for 1)))
  (advice-add 'transmission :before 'ambrevar/transmission-start-daemon)
  (setq transmission-refresh-modes '(transmission-mode transmission-files-mode transmission-info-mode transmission-peers-mode)
        transmission-refresh-interval 1))

;;; Theme
(cond
 ((ignore-errors (load-theme 'cyberpunk :no-confirm))
  ;; Backport unmerged changes.  See
  ;; https://github.com/n3mo/cyberpunk-theme.el/issues/46.
  (set-face-attribute 'lazy-highlight nil
                      :underline '(:color "yellow")
                      :foreground 'unspecified
                      :background 'unspecified)
  (with-eval-after-load 'magit
    (let ((cyberpunk-green-2 "#006400"))
      (set-face-background 'diff-refine-added cyberpunk-green-2)))
  (with-eval-after-load 'org
    (set-face-attribute 'org-level-1 nil :height 1.1)
    (set-face-attribute 'org-level-2 nil :height 1.0)
    (set-face-attribute 'org-level-3 nil :height 1.0)))

 ((require 'doom-themes nil :noerror)
  (load-theme 'doom-one :no-confirm))

 (require 'theme-ambrevar))
;;; Modeline.
(when (require 'doom-modeline nil :noerror)
  (doom-modeline-mode))

;;; Translator
(when (require 'google-translate nil t)
  (require 'google-translate-default-ui)
  ;; (global-set-key "\C-ct" 'google-translate-at-point)
  ;; (global-set-key "\C-cT" 'google-translate-query-translate)
  (defun ambrevar/google-translate-line ()
    "Translate current line and insert result after it, separated by ' = '."
    (interactive)
    (let* ((langs (google-translate-read-args nil nil))
           (source-language (car langs))
           (target-language (cadr langs))
           text
           result)
      (end-of-line)
      (just-one-space)
      (setq text (buffer-substring-no-properties
                  (line-beginning-position) (line-end-position)))
      (setq result (with-temp-buffer
                     (google-translate-translate
                      source-language target-language
                      text
                      'current-buffer)
                     (buffer-string))
            (insert "= " result)))))

(when (require 'wgrep nil t)
  ;; TODO: wgrep-face is not so pretty.
  (set-face-attribute 'wgrep-face nil :inherit 'ediff-current-diff-C :foreground 'unspecified :background 'unspecified :box nil))

;;; Window manager
(with-eval-after-load 'pulseaudio-control
  (setq pulseaudio-control-use-default-sink t
        pulseaudio-control-volume-step "2%"))
(with-eval-after-load 'exwm
  (require 'init-exwm))

;;; XML / SGML
(defun ambrevar/sgml-setup ()
  (setq sgml-xml-mode t)
  ;; (toggle-truncate-lines) ; This seems to slow down Emacs.
  (turn-off-auto-fill))
(add-hook 'sgml-mode-hook 'ambrevar/sgml-setup)
(with-eval-after-load 'nxml-mode
  (set-face-foreground 'nxml-element-local-name "gold1")
  (defvaralias 'nxml-child-indent 'tab-width))
;;; Because XML is hard to read.
(add-hook 'nxml-mode-hook 'ambrevar/turn-on-tab-width-to-4)

(with-eval-after-load 'youtube-dl
  (setq youtube-dl-directory "~/Downloads"))
(with-eval-after-load 'ytdl
  (setq ytdl-always-query-default-filename 'yes)
  (setq ytdl-max-mini-buffer-download-type-entries 0)
  (setq ytdl-download-extra-args '("-f" "best"))
  (require 'patch-ytdl)
  (defun ytdl-no-proxy ()
    (interactive)
    (let ((ytdl-download-extra-args (cons "--proxy="
                                          ytdl-download-extra-args)))
      (ytdl-download))))

(with-eval-after-load 'ztree
  (set-face-foreground 'ztreep-diff-model-add-face "deep sky blue"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Finalization


(load (expand-file-name "hackpool/hacks.el" (getenv "PERSONAL")) :noerror)

;;; Don't let `customize' clutter my config.
(setq custom-file
      (if (boundp 'server-socket-dir)
          (expand-file-name "custom.el" server-socket-dir)
        (expand-file-name (format "emacs-custom-%s.el" (user-uid)) temporary-file-directory)))
(load custom-file t)

;;; Local config. You can use it to set system specific variables, such as the
;;; external web browser or the geographical coordinates:
;;
;; (setq calendar-latitude 20.2158)
;; (setq calendar-longitude 105.938)
(load "local-after" t)
