;; Useful to navigate mailist archives.

;; M-x gnus
;; M-x gnus-group-enter-server-mode
;; Subscribe to desired lists and go back to groups.

(with-eval-after-load 'gnus
  (setq gnus-select-method '(nnnil)
        gnus-expert-user t
        gnus-always-read-dribble-file t
        gnus-thread-sort-functions '(gnus-thread-sort-by-most-recent-number)
        gnus-article-sort-functions gnus-thread-sort-functions
        gnus-secondary-select-methods
        '((nntp "news.gmane.io")))
  (setq nnir-method-default-engines '((nntp . gmane) (nnmaildir . notmuch)))
  (add-hook 'gnus-group-mode-hook 'gnus-topic-mode))

(provide 'init-gnus)
