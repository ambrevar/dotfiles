(specifications->manifest               ; TODO: Replace whole profile with tectonic?
  '("texlive-amsfonts"
    "texlive-base"
    "texlive-fonts-ec"
    "texlive-generic-ifxetex"
    "texlive-generic-ulem"
    "texlive-latex-capt-of"
    "texlive-latex-enumitem"
    "texlive-latex-eukdate"
    "texlive-latex-geometry"
    "texlive-latex-hyperref"
    "texlive-latex-koma-script"
    "texlive-latex-l3kernel"
    "texlive-latex-l3packages"
    "texlive-latex-media9"
    "texlive-latex-ms"
    "texlive-latex-needspace"
    "texlive-latex-oberdiek"
    "texlive-latex-ocgx2"
    "texlive-latex-pgf"
    "texlive-latex-wrapfig"
    "texlive-url"
    "texlive-xcolor"))
