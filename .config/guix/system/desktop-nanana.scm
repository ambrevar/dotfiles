;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/desktop-nanana.scm
(define-module (desktop-nanana)
  #:use-module (srfi srfi-1)
  #:use-module (default)
  #:use-module (guix packages)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services))

(use-service-modules
 desktop                                ; To remove bluetooth.
 networking                             ; To remove NetworkManager.
 ;; To remove GDM:
 xorg)

(use-package-modules
 ;; To remove iw.
 linux)

(define-public %nanana/services
  (cons*
   (service connman-service-type)
   (service slim-service-type (slim-configuration
                               (display ":0")
                               (vt "vt7")))
   (remove
    (lambda (service)
      (or
       (member (service-kind service)
               (list bluetooth-service
                     cups-pk-helper-service-type
                     geoclue-service
                     gdm-service-type
                     ;; NetworkManager.  The applet is a
                     ;; simple-service and must be removed by
                     ;; matching the type name.
                     network-manager-service-type
                     ;; wpa-supplicant-service-type ; Still needed for networking.
                     modem-manager-service-type
                     usb-modeswitch-service-type))
       (member (service-type-name (service-kind service))
               '(network-manager-applet))))
    %ambrevar/services)))

(define-public %nanana/os
  (operating-system
    (inherit %ambrevar/default-os)
    (host-name "nanana")

    (file-systems (cons* (file-system
                           (device (uuid "CAE6-BFFA" 'fat))
                           (mount-point "/boot/efi")
                           (type "vfat"))
                         %ambrevar-file-systems))

    (packages (delete iw (operating-system-packages %ambrevar/default-os)))

    (services %nanana/services)))

%nanana/os
