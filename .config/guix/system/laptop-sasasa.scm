;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/laptop-sasasa.scm
(define-module (laptop-sasasa)
  #:use-module (srfi srfi-1)
  #:use-module (default)
  #:use-module (nongnu packages linux)
  #:use-module (gnu)
  #:use-module (gnu system))

(use-service-modules
 desktop                                ; GNOME
 ;; GDM:
 xorg)

(use-package-modules
 emacs-xyz                              ; For exwm config.
 ;; For Xorg modules:
 xorg)

(define %sasasa/services
  (cons*
   ;; TODO: How can I choose which display to focus by default?  Order seems not
   ;; to matter.
   (service slim-service-type
            (slim-configuration
             (display ":0")
             (vt "vt7")))
   (service slim-service-type
            (slim-configuration
             (display ":1")
             (vt "vt8")
             (auto-login? #f)
             (default-user "ambrevar")
             (auto-login-session (file-append emacs-exwm-no-x-toolkit
                                              "/bin/exwm"))))
   (modify-services
       (remove (lambda (service)
                 (eq? (service-kind service) gdm-service-type))
               %ambrevar/services)
     (udev-service-type config =>
                        (udev-configuration
                         (inherit config)
                         (rules (append (udev-configuration-rules config)
                                        (list %ambrevar/backlight-udev-rule))))))))

(operating-system
  (inherit %ambrevar/default-os)
  (host-name "sasasa")
  (kernel linux)

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (timeout 1)
               (target "/dev/sda")))

  (file-systems (cons* (file-system
                         (device (file-system-label "guix"))
                         (mount-point "/")
                         (type "btrfs")
                         (options "subvol=rootfs,compress=zstd"))
                       (file-system
                         (mount-point "/tmp")
                         (device "none")
                         (type "tmpfs")
                         (check? #f))
                       %base-file-systems))

  (services (cons* (service gnome-desktop-service-type
                            (gnome-desktop-configuration
                             (gnome gnome-minimal)))
                   %sasasa/services)))
