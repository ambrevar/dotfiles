;; To collect the size of a system:
;; guix size $(guix system -L ~/.config/guix/system build ~/.config/guix/system/default.scm)
(define-module (default)
  #:use-module (srfi srfi-1)
  #:use-module (gnu)
  #:use-module (gnu system nss)
  #:use-module (gnu system mapped-devices)
  #:use-module (guix packages))

(use-service-modules
 desktop
 ;; For tor
 networking)

(use-package-modules
 certs                                  ; nss-certs
 gnome                                  ; For gnome-minimal
 linux                                  ; ntfs-3g
 mtools                                 ; exfat
 nano                                   ; To remove nano.
 emacs                                  ; For emacs-no-x-toolkit
 emacs-xyz                              ; EXWM
 xorg                                   ; For emacs-no-x-toolkit
 ;; To remove zile:
 zile)

(define %ambrevar/cdemu-vhba-udev-rule
 ;; For the "uaccess" tag to be applied properly, the rule must be executed
 ;; before the uaccess rule
 ;; (/run/current-system/profile/lib/udev/rules.d/70-uaccess.rules).
  (udev-rule
   "69-cdemu-vhba.rules"
   (string-append "KERNEL==\"vhba_ctl\", SUBSYSTEM==\"misc\", TAG+=\"uaccess\"")))

(define-public %ambrevar/xorg-touchpad
  "Section \"InputClass\"
  Identifier \"Touchpads\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsTouchpad \"on\"

  Option \"DisableWhileTyping\" \"on\"
  Option \"MiddleEmulation\" \"on\"
  Option \"ClickMethod\" \"clickfinger\"
  Option \"ScrollMethod\" \"twofinger\"
  Option \"NaturalScrolling\" \"true\"
EndSection")

(define-public %ambrevar/backlight-udev-rule
  ;; Allow members of the "video" group to change the screen brightness.
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

;; Mount Nitrokey
;; TODO: Check if plugdev works instead of users.  If not, report to Nitrokey.
;; https://www.nitrokey.com/sites/default/files/41-nitrokey.rules
(define %nitrokey-udev-rule
  (udev-rule
   "41-nitrokey.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"usb\", "
                  "ATTR{idVendor}==\"20a0\", ATTR{idProduct}==\"4211\", "
                  "ENV{ID_SMARTCARD_READER}=\"1\", ENV{ID_SMARTCARD_READER_DRIVER}=\"gnupg\", GROUP+=\"users\", MODE=\"0666\"")))
;; (define %nitrokey-udev-rule
;;   (file->udev-rule
;;    "41-nitrokey.rules"
;;    (let ((version "20170910"))
;;      (origin
;;        (method url-fetch)
;;        (uri "https://www.nitrokey.com/sites/default/files/41-nitrokey.rules")
;;        (sha256
;;         (base32 "127nghkfd4dl5mkf5xl1mij2ylxhkgg08nlh912xwrrjyjv4y9sa"))))))

(define-public %ambrevar/tor-config
  "ExitNodes {se},{nl},{fr},{ee},{no},{dk},{fi}
StrictNodes 1")

(define-public %ambrevar/services
  (cons*
   (service tor-service-type
            (tor-configuration
             (config-file (plain-file "tor.conf" %ambrevar/tor-config))))
   ;; Use the "desktop" services, which include the X11 log-in service, networking
   ;; with Wicd, and more.
   (modify-services
       %desktop-services
     (guix-service-type config =>
                        (guix-configuration
                         (inherit config)
                         (discover? #t)
                         ;; Don't clean build deps.
                         ;; See (info "(guix) Invoking guix-daemon").
                         ;; WARNING: This tends to yield an ever-growing store.
                         ;; (extra-options '("--gc-keep-outputs"))
                         (substitute-urls (append
                                           (@@ (guix scripts substitute) %default-substitute-urls)
                                           ;; TODO: Does not work?
                                           ;; Nonguix:
                                           (list "https://mirror.brielmaier.net")))
                         (authorized-keys (append
                                           %default-authorized-guix-keys
                                           (list (local-file "mirror.brielmaier.net.pub"))))))
     (udev-service-type config =>
                        (udev-configuration
                         (inherit config)
                         (rules (append (udev-configuration-rules config)
                                        (list ;; %nitrokey-udev-rule
                                         %ambrevar/cdemu-vhba-udev-rule))))))))

(define-public gnome-minimal
  (package
    (inherit gnome)
    (name "gnome-minimal")
    (propagated-inputs
     ;; Keep nautilus.
     (fold alist-delete (package-propagated-inputs gnome)
           '(;; GNOME-Core-Shell
             "gnome-backgrounds"
             "gnome-themes-extra"
             "gnome-getting-started-docs"
             "gnome-user-docs"
             ;; GNOME-Core-Utilities
             "baobab"
             "cheese"
             "eog"
             "epiphany"
             "evince"             ; TODO: Remains pulled in with gnome-default-applications.
             "file-roller"
             "gedit"
             "gnome-boxes"
             "gnome-calculator"
             "gnome-calendar"
             "gnome-characters"
             "gnome-clocks"
             "gnome-contacts"
             "gnome-disk-utility"
             "gnome-font-viewer"
             "gnome-maps"
             ;; "gnome-music"
             ;; "gnome-photos"
             "gnome-screenshot"
             ;; "gnome-system-monitor" ; TODO: Needed for gnome-polkit-settings: edit package to no break when gnome-system-monitor is missing.
             "gnome-terminal"
             "gnome-weather"
             "simple-scan"
             "totem"
             ;; Others
             "gnome-online-accounts")))))

(define-public %ambrevar/packages
  (cons* nss-certs          ; for HTTPS access
         ;; gvfs            ; TODO: For user mounts?
         ntfs-3g
         ;; exfat-utils     ; TODO: Needed for macOS drives?  Does not seem to work.
         fuse-exfat
         emacs-exwm-no-x-toolkit   ; Still needs emacs-exwm / emacs-exwm-no-x-toolkit installed in a user profile.
         vhba-module        ; For CDEmu.
         (fold (lambda (package l) (delete package l))
               %base-packages
               (list nano zile
                     ;; wireless-tools is deprecated in favour of iw.
                     wireless-tools))))

(define-public %ambrevar/firmware
  %base-firmware)

(define-public %ambrevar/user
  (user-account
   (name "ambrevar")
   (group "users")
   (supplementary-groups '("wheel" "netdev" ; netdev is needed for networking.
                           "kvm"            ; For QEMU (and maybe libvirt)
                           ;; "plugdev"     ; TODO: Needed for nitrokey?
                           "lp"         ; TODO: Needed for bluetooth?
                           "video"))
   ;; TODO: Can we default to name?
   (home-directory "/home/ambrevar")))

(define-public %ambrevar/file-systems
  (cons* (file-system
           (device (file-system-label "guix"))
           (mount-point "/")
           (type "btrfs")
           (options "subvol=rootfs,compress=zstd"))
         (file-system
           (mount-point "/tmp")
           (device "none")
           (type "tmpfs")
           (check? #f))
         %base-file-systems))

(define-public %ambrevar/default-os
  (operating-system
    (host-name "ambrevar-system")
    (timezone "Europe/Paris")
    (locale "en_US.utf8")

    ;; Use the UEFI variant of GRUB with the EFI System
    ;; Partition mounted on /boot/efi.
    (bootloader (bootloader-configuration
                 (bootloader grub-efi-bootloader)
                 (timeout 1)
                 (target "/boot/efi")))

    (firmware %ambrevar/firmware)
    ;; TODO: Remove all virtio modules?
    ;; (initrd-modules (delete "virtio-rng" %base-initrd-modules))

    ;; WARNING: Each EFI system must set their own EFI partition!
    (file-systems (cons* (file-system
                           (device (uuid "CAE6-BFFA" 'fat))
                           (mount-point "/boot/efi")
                           (type "vfat"))
                         %ambrevar/file-systems))

    (users (cons* %ambrevar/user
                  %base-user-accounts))

    (packages %ambrevar/packages)

    (services %ambrevar/services)

    ;; Allow resolution of '.local' host names with mDNS.
    (name-service-switch %mdns-host-lookup-nss)))

%ambrevar/default-os
