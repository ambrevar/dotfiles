;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/desktop-fafafa.scm

;; WARNING: Until https://issues.guix.info/issue/37305#16 is merged, btrfs
;; subvolume are not supported by GRUB in Guix.
;; Temporary workaround is to add the following entry to GRUB after a "guix system reconfigure":
;;
;; menuentry "GNU with Linux 5.4.25" {
;;   search --label --set guix
;;   linux /rootfs/gnu/store/cwyag3gnahmibm0rg1f0j8bn3xw9k8fi-linux-5.4.25/bzImage --root=guix --system=/gnu/store/h5fkddrfrii7254d67aslicjy9karvpa-system --load=/gnu/store/h5fkddrfrii7254d67aslicjy9karvpa-system/boot quiet
;;   initrd /rootfs/gnu/store/zlqn6hb9w7yqzsc08kfljijw55jx5j3x-raw-initrd/initrd.cpio.gz
;; }
;;
;; Note that /rootfs ire prefixed to the kernel and initrd paths, but not to the arguments.

(define-module (desktop-fafafa)
  #:use-module (nongnu packages linux)
  #:use-module (default)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services))

(use-service-modules
 cups                                   ; Printing
 desktop                                ; GNOME
 nix
 ssh
 virtualization                         ; libvirt
 ;; GDM:
 xorg)

(use-package-modules
 linux
 ;; nix:
 package-management)

(define %fafafa/services
  (cons*
   (service nix-service-type)
   (modify-services
       %ambrevar/services
     (gdm-service-type config =>
                       (gdm-configuration
                        (inherit config)
                        (default-user "francoise")
                        ;; (auto-login? #t) ; TODO: This breaks GDM, fix it!
                        )))))

(operating-system
  (inherit default-operating-system)
  (host-name "fafafa")
  (keyboard-layout (keyboard-layout "fr"))

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (timeout 1)
               (target "/dev/sda")))

  ;; Needed for r600 Radeon module.  With nonfree Linux only, X won't start.
  (kernel linux)
  (firmware (append (list radeon-firmware)
                    %ambrevar/firmware))

  (file-systems (cons* (file-system
                         (device (file-system-label "guix"))
                         (mount-point "/")
                         (type "btrfs")
                         (options "subvol=rootfs,compress=zstd"))
                       (file-system
                         (device (file-system-label "data"))
                         (mount-point "/media/data")
                         (type "ext4"))
                       (file-system
                         (mount-point "/tmp")
                         (device "none")
                         (type "tmpfs")
                         (check? #f))
                       %base-file-systems))

  (users (cons* (user-account
                 (name "francoise")
                 (group "users")
                 (supplementary-groups '("netdev" ; netdev is needed for networking.
                                         "kvm"    ; For QEMU (and maybe libvirt)
                                         "video"))
                 (home-directory "/home/francoise"))
                (operating-system-users default-operating-system)))

  (packages (cons* nix
                   (operating-system-packages default-operating-system)))

  (services (cons*
             (set-xorg-configuration
              (xorg-configuration
               (keyboard-layout keyboard-layout)))
             (service gnome-desktop-service-type)
             (service libvirt-service-type
                      (libvirt-configuration
                       (unix-sock-group "kvm")))
             (service openssh-service-type
                      (openssh-configuration
                       (x11-forwarding? #t)
                       (password-authentication? #f)))
             (service cups-service-type
                      (cups-configuration
                       (web-interface? #t)))
             %fafafa/services)))
