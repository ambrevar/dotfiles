;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/laptop-mimimi.scm
(define-module (laptop-mimimi)
  #:use-module (srfi srfi-1)
  #:use-module (default)
  #:use-module (nongnu packages linux)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services)
  #:use-module (guix download) ; For linux-xiaomi-air-13
  #:use-module ((guix utils) :prefix guix-utils:)
  #:use-module (guix packages))

(use-service-modules
 desktop                                ; GNOME
 ;; GDM:
 xorg)

(use-package-modules
 emacs-xyz                              ; For exwm config.
 gnome                                  ; gnome-minimal
 linux                                  ; For custom kernel.
 ;; For Xorg modules:
 xorg)

(define linux-xiaomi-air-13
  (package
    (inherit linux)
    (name "linux-xiaomi-air-13")
    ;; To build a custom kernel, pass it an alternate "kconfig":
    (native-inputs
     `(("kconfig" ,(local-file "./linux-laptop.conf"))
       ,@(alist-delete "kconfig" (package-native-inputs linux-libre))))))

(define %mimimi/power-tweaks
  ;; TODO: The following service starts too soon and results in a kernel panic
  ;; because /sys/... is not found.
  (simple-service
   'my-/sys-tweaks activation-service-type
   ;; >> echo '1' > '/sys/module/snd_hda_intel/parameters/power_save';
   #~(call-with-output-file "/sys/module/snd_hda_intel/parameters/power_save"
       (lambda (port)
         (display "1" port)))
   ;; >> echo 'auto' > '/sys/bus/usb/devices/1-6/power/control';
   ;; >> echo 'auto' > '/sys/bus/usb/devices/1-7/power/control';
   ;; >> echo 'auto' > '/sys/bus/i2c/devices/i2c-2/device/power/control';
   ;; >> echo 'auto' > '/sys/bus/pci/devices/0000:02:00.0/power/control';
   ))

(define %mimimi/xorg-minimal-config
  (xorg-configuration
               (modules (list xf86-video-vesa
                              xf86-video-intel
                              ;; xf86-video-nouveau ; Make sure this is on if the 'nouveau' kernel module is loaded.
                              xf86-input-libinput))
               (extra-config (list %ambrevar/xorg-touchpad))))

(define %mimimi/services
  (cons*
   ;; TODO: How can I choose which display to focus by default?  Order seems not
   ;; to matter.
   (service slim-service-type
            (slim-configuration
             (display ":0")
             (vt "vt7")
             (xorg-configuration %mimimi/xorg-minimal-config)))
   (service slim-service-type
            (slim-configuration
             (display ":1")
             (vt "vt8")
             (auto-login? #t)
             (default-user "ambrevar")
             (auto-login-session (file-append emacs-exwm-no-x-toolkit
                                              "/bin/exwm"))
             (xorg-configuration %mimimi/xorg-minimal-config)))
   (modify-services
       (remove (lambda (service)
                 (eq? (service-kind service) gdm-service-type))
               %ambrevar/services)
     (udev-service-type config =>
                        (udev-configuration
                         (inherit config)
                         (rules (append (udev-configuration-rules config)
                                        (list %ambrevar/backlight-udev-rule))))))))

(define drive-mapping
  (list (mapped-device
         ;; The UUID is that returned by 'cryptsetup luksUUID'.
         (source (uuid "17d4df8c-04dc-43aa-b017-f69cd3bc5a7d"))
         (target "guix")
         (type luks-device-mapping))))

(define root-partition
  (file-system
    (device (file-system-label "guix"))
    (mount-point "/")
    (type "btrfs")
    (options "subvol=rootfs,compress=zstd")
    (dependencies drive-mapping)))

(define-public %ambrevar/laptop-os
  (operating-system
    (inherit %ambrevar/default-os)
    (host-name "mimimi")

    (kernel linux)
    ;; (kernel linux-xiaomi-air-13)
    ;; To disable the Nvidia, first make sure nouveau is not loaded then call
    ;;   sudo tee /proc/acpi/bbswitch <<<OFF
    ;; Check the result with
    ;;   sudo dmesg | grep bbswitch
    ;; It saves about 1h of battery.
    ;; TODO: Automate the bbswitch command when the Guix package is merged.
    (kernel-arguments '("modprobe.blacklist=nouveau"))

    (mapped-devices drive-mapping)

    (firmware (append (list iwlwifi-firmware)
                      %ambrevar/firmware))

    (file-systems (cons* root-partition
                         ;; TODO: Separate boot partition (to avoid double LUKS
                         ;; password prompt) does not work:
                         ;; (file-system
                         ;;   (device (file-system-label "boot"))
                         ;;   (mount-point "/boot")
                         ;;   (type "ext4")
                         ;;   ;; (needed-for-boot? #t)
                         ;;   (dependencies (list root-partition))
                         ;;   )
                         (file-system
                           (device (uuid "4E30-891F" 'fat))
                           (mount-point "/boot/efi")
                           (type "vfat"))
                         (file-system
                           (mount-point "/tmp")
                           (device "none")
                           (type "tmpfs")
                           (check? #f))
                         %base-file-systems))

    (services (cons* (service gnome-desktop-service-type
                              (gnome-desktop-configuration
                               (gnome gnome-minimal)))
                     %mimimi/services))))

%ambrevar/laptop-os
