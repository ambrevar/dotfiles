(use-modules (srfi srfi-1)
             (ice-9 textual-ports)
             (ice-9 popen))

(define (find-guix-channel channel-list)
  (find
   (lambda (channel) (eq? (channel-name channel) 'guix))
   channel-list))

(define current-guix (find-guix-channel %default-channels))

(define (current-guix-commit)
  (let* ((port (open-input-pipe "guix describe -f channels"))
         (str (get-string-all port)))
    (close-pipe port)
    (channel-commit
     (find-guix-channel (eval-string str)))))

(define* (channel-list #:key guix-commit)
  (cons*
   (channel
    (name 'nonguix)
    (url "https://gitlab.com/nonguix/nonguix.git")
    (introduction
     (make-channel-introduction
      "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
      (openpgp-fingerprint
       "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5")))
    (branch "master"))
   (channel
    (name 'guix-gaming-games)
    (url "https://gitlab.com/guix-gaming-channels/games.git")
    ;; (url "/home/ambrevar/projects/games")
    (introduction
     (make-channel-introduction
      "c23d64f1b8cc086659f8781b27ab6c7314c5cca5"
      (openpgp-fingerprint
       "50F3 3E2E 5B0C 3D90 0424  ABE8 9BDC F497 A4BB CC7F")))
    (branch "master"))

   ;; (channel
   ;;  (name 'guix-gaming-duke-nukem-3d)
   ;;  (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
   ;;  (branch "master"))

   ;; (channel
   ;;  (name 'guix-gaming-quake-3)
   ;;  (url "https://gitlab.com/guix-gaming-channels/quake-3.git")
   ;;  (branch "master"))

   (cons
    (if guix-commit
        (channel
         (inherit current-guix)
         (commit guix-commit))
        current-guix)
    (delete current-guix %default-channels))))

;; (channel-list #:guix-commit (current-guix-commit))
(channel-list)
